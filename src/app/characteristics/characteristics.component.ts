import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-characteristics',
  templateUrl: './characteristics.component.html',
  styleUrls: ['./characteristics.component.css']
})
export class CharacteristicsComponent implements OnInit {

  dependableCollapsed = false;
  responsibleCollapsed = false;
  starterCollapsed = false;
  accountableCollapsed = false;
  integrityCollapsed = false;
  workerCollapsed = false;
  quickCollapsed = false;
  teachableCollapsed = false;
  detailCollapsed = false;
  teamworkCollapsed = false;
  motivatedCollapsed = false;
  enthusiasticCollapsed = false;
  resilientCollapsed = false;
  persistentCollapsed = false;
  solverCollapsed = false;

  constructor() { }

  ngOnInit() {
  }

}
