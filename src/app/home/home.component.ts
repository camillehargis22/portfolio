import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  aboutCollapsed = false;
  experienceCollapsed = false;
  projectsCollapsed = false;
  skillsCollapsed = false;
  characteristicsCollapsed = false;
  awardsCollapsed = false;
  educationCollapsed = false;
  contactCollapsed = false;
  photoCollapsed = false;

  constructor() { }

  ngOnInit() {
  }

}
